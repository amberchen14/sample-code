Two interface, one is traffic measurement, the other is traffic simulation.
## Traffic measurement 
Input tab includes start/end sensor, analysis time period and historical time period, and parameter selection. 
![measure_input](/User Interface/measurement_input.png "Traffic measurement input user interface")
<br>
<br>
### Data available calendar
Most of outputs are displayed in the folder of "Calculation of work zone impact". One of the interesting output is data available ratio displaying on the calendar. 
The calendar has shown in three aggregation, 
1. The aggregation of total selected sensors
2. The aggregation of selected sensors by direction
3. One selected sensor on the map. 

The left calendar is the total selected sensor, and the right calendar is shown by direction.
![measure_output1](/User Interface/measurement_data_availability_calendar1.png "Data available ratio 1")
<br>
The calendar below shows data availability from one selected sensor. User can choose the sensor with the steps below,
1. Choose the direction
2. click the sensor on the map. 
![measure_output1](/User Interface/measurement_data_availability_calendar2.png "Data available ratio 2")

<br>

## Traffic simulation 
Tab 1 includes subarea, closure, analysis time period and historical time period. 
![simulate_input1](/User Interface/simulation_input_subarea.png "input_subarea")
<br>
Tab 2 includes data source of mainlane and ramp flow, and parameter selection (tab2).
![simulate_input2](/User Interface/simulation_input_flow.png "input_flow")
<br>
The simulation output is shown below,
<br>
![simulate_output](/User Interface/simulation_output.png "output_heatmap")


