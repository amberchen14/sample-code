#Trace bus location in CORSIM network very second. 
void CNetwork::TraceBusLocation()
{
   //sprintf_s( gsOutput, "TraceEVLocation was called\n" );
   //OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );
   //loop through vehicles to find all EVs
	for (int iv = 0; iv < ttlveh; iv++ )
	{
	   if( nfleet[iv] == 3 && nvhlnk[iv] > 0)
	   {
		   int LinkID = lastlnk[iv]-1;
		   int ttime = sclock - entime[iv] / 10 + 1;
		   int Dis2Sig = xlngth1[LinkID] - distup[iv]; 


		   sprintf_s( gsOutput, "Bus ID = %d, Time = %d seconds, Speed = %d feet/sec, Travel time = %d seconds, link number %d ",
			   netgvh[iv], sclock, spdln[iv], ttime , LinkID );
		   OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );

		   sprintf_s( gsOutput, "link length %d , length from upnode = %d , distance to signal %d ", xlngth1[LinkID],distup[iv], Dis2Sig  );
		   OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );
	   }
   }
} 

#Update signal time every second.
#When the bus approaches the intersection, the signal decides green extension/red trunction

void CNetwork::UpdateBusPriorityNodeSignalStates()
{
   // Update the signal states for all the
   // nodes not controlled by CORSIM.
   int nTime = sclock + giEndOfInit;

   CNode* pNode = NULL;
   POSITION pos = m_NodeList.GetHeadPosition();
   while( pos != NULL )
   {
      pNode = m_NodeList.GetNext( pos );
      if( pNode->GetControlType() == CString("external") )
      {
		  int node = pNode->GetID();
		  for (int n = 0; n < signal; n ++)
		  {
			if (node == cycle[n][0])
			{
				if(cycle[n][1] == 0)
				{
					ResetSignalTime(n);

					pNode->SetSignalState();

				}
				else
				{
					for (int m = 1; m < interval+1; m ++)
					{
						if(signalduration[n][m] < Record_signalduration[n][m] && signalduration[n][m] > 0 )
						{
							signalduration[n][m]--;
							break;
						}
						else if(signalduration[n][m] == Record_signalduration[n][m]  )
						{
							for (int j = 1; j < approachnode + 1; j ++)
							{
								SetSignal( upstreamnode[n][j], upstreamnode[n][0],
									signalsign[n][m][1],
									pSignalState->GetThrough(),
									pSignalState->GetRight(),
									pSignalState->GetLeftDiag() );
							}
			int upstreamnode[signal][approachnode + 1]; // 0 node, 2~5 approach node		
			int signalsign[signal][interval + 1][approachnode]; // 0 = node, 1~interval+1 = interval
							signalduration[n][m]--;
							break;
						}
					}
					
				}
				cycle[n][1]--;
			}
		  }

	  }
   }
}