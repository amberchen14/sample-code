#Calculate closure impact using sensor data
#vermac_refer: sensor data in typical day, including sensor name, time, count and speed
#vermac_closure: sensor data in closure day
#ver_pos: sensor positions and relative distance
#spd_threshold: speed threshold for defining queue 
#time ragne: closure time range
#direct: selected sensor direction

closure_impact_sensor<-function(vermac_refer, vermac_closure, ver_pos, spd_threshold, time_range, direct)
  threshold<-spd_threshold
  queue_length<-data.frame() 
  queue_position<-data.frame()
  delay_time<-data.frame()
  travel_time<-data.frame()
  seg_speed<-data.frame()
  
  vermac_speed_plot<-vermac_closure
  vermac_speed_plot<-vermac_speed_plot[c("time", "speed", "sensor", "dir")]
  vermac_speed_plot<-merge(x=vermac_speed_plot, y=ver_pos, by.x=c("sensor", "dir"), by.y=c("present_name", "dir"), all.x=TRUE)
  vermac_speed_plot<-vermac_speed_plot[c("sensor", "dir", "time", "speed", "dis")]
  
  vermac_count_plot<-filter(vermac_closure,vermac_closure$time %in% time_range)
  vermac_count_plot<-filter(vermac_count_plot, !is.na(vermac_count_plot$speed))
  vermac_count_plot<-vermac_count_plot[c("time", "count","speed", "sensor" ,"dir")]
    
  #flow plot
  vermac_count_plot$category<-cut(vermac_count_plot$speed,
                                  breaks=speed_range,
                                  labels=speed_range[1:(length(speed_range)-1)])
  vermac_count_plot$category<-as.integer(vermac_count_plot$category)

  #for vermac queue estimation
  for (d in 1:length(direct))
  {
    ver_pos_study<-filter(ver_pos, ver_pos$dir==direct[d])
    ver_duplicate<-ver_pos_study[c("present_name", "dir")]
    ver_pos_study<-ver_pos_study[!duplicated(ver_duplicate), ]   
    ver_pos_study<-ver_pos_study[c("present_name", "dir", "lon", "lat", "dis")]
    names(ver_pos_study)<-c("sensor", "dir", "lon", "lat", "dis")
    ver_pos_study<-ver_pos_study[order(ver_pos_study$dis), ]
    ver_pos_relative_mile<-ver_pos_study$dis
    
    ver_pos_dis<-c(ver_pos_study$dis[2:nrow(ver_pos_study)])
    date_tra<-filter(vermac_closure, vermac_closure$dir %in% direct[d])
    date_speed_raw<-date_tra[c("time", "speed", "sensor")]
    date_speed<-data.frame(time=time_range)
    q_length<-data.frame(time=time_range, length = 0, direct=direct[d])
    seg_date_speed<-data.frame(time=time_range)
    seg_length<-c()
    
    ver_pos_dis<-c(ver_pos_study$dis[2:nrow(ver_pos_study)])
    date_tra<-filter(vermac_closure, vermac_closure$dir %in% direct[d])
    tra_normal<-filter(vermac_refer, vermac_refer$dir %in% direct[d])
    
    date_speed_raw<-date_tra[c("time", "speed", "sensor")]
    normal_speed_raw<-tra_normal[c("time", "speed", "sensor")]
    normal_speed<-data.frame(time=strftime(time_range, format="%H:%M"))    
    date_speed<-data.frame(time=time_range)
    q_length<-data.frame(time=time_range, length = 0, direct=direct[d])
    seg_date_speed<-data.frame(time=time_range)
    seg_normal_speed<-data.frame(time=time_range)
    seg_length<-c()
    
    date_travel_time<-data.frame(time=time_range)
    normal_travel_time<-data.frame(time=time_range)  
    
    #transform data format, old = time, sensor name, speed; new column name: time, distance (cell=speed), 
    for (i in 1:nrow(ver_pos_study))
    {
      df<-filter(date_speed_raw, date_speed_raw$sensor==ver_pos_study$sensor[i])
      if (nrow(df)==0) 
      {
        df<-data.frame(speed=NA)
        date_speed<-cbind(date_speed, df)
        next
      }
      df<-aggregate(df$speed, by=list(df$time), FUN=mean, na.rm=TRUE)
      names(df)<-c("time", ver_pos_study$dis[i])
      date_speed<-merge(x=date_speed, y=df,by.x="time", by.y="time", all.x=TRUE, sort= FALSE)
    }
    
    for (i in 1:nrow(ver_pos_study))
    {     
      df<-filter(normal_speed_raw, normal_speed_raw$sensor==ver_pos_study$sensor[i])
      if (nrow(df)==0)       
      {
        df<-data.frame(speed=NA)
        normal_speed<-cbind(normal_speed, df)
        next
      }
      
      df<-aggregate(df$speed, by=list(strftime(df$time, format="%H:%M")), FUN=mean, na.rm=TRUE)
      names(df)<-c("time", ver_pos_study$dis[i])
      normal_speed<-merge(x=normal_speed, y=df,by.x="time", by.y="time", all.x=TRUE, sort= FALSE)
    }
    names(date_speed)<-c("time", ver_pos_relative_mile)
    date_speed<-date_speed[order(date_speed$time), ]
    
    
    df<-array(c(data.matrix(date_speed[2: (ncol(date_speed)-1)]),  
                data.matrix(date_speed[3: ncol(date_speed)])), c(nrow(date_speed), (ncol(date_speed)-2), 2))
    df<-apply(df,c(1,2),mean, na.rm=TRUE)
    seg_date_speed<-data.frame(time_range, df)
    seg_length<-(ver_pos_relative_mile[2:length(ver_pos_relative_mile)] - ver_pos_relative_mile[1:(length(ver_pos_relative_mile)-1)])
    names(seg_date_speed)<-c("time", seg_length)
    
    df<-array(c(data.matrix(normal_speed[2: (ncol(normal_speed)-1)]),  
                data.matrix(normal_speed[3: ncol(normal_speed)])), c(nrow(normal_speed), (ncol(normal_speed)-2), 2))
    df<-apply(df,c(1,2),mean, na.rm=TRUE)
    seg_normal_speed<-data.frame(time_range, df)
    names(seg_normal_speed)<-c("time", seg_length)
    
    date_travel_time<-data.frame()
    normal_travel_time<-data.frame()
    for (i in 2: ncol(seg_normal_speed)){
      sensor_relative_distance<-(ver_pos_relative_mile[i]-ver_pos_relative_mile[i-1])
      df<-data.frame(time=seg_date_speed$time, speed=seg_date_speed[,i], length=sensor_relative_distance)
      date_travel_time<-rbind(date_travel_time, df)
      
      df<-data.frame(time=seg_normal_speed$time, speed=seg_normal_speed[,i],length=sensor_relative_distance)
      normal_travel_time<-rbind(normal_travel_time, df)
    }
    # queue length
    for (i in 2:ncol(seg_date_speed))
    {
      a<-which(seg_date_speed[i]<=threshold)
      q_length$length[a]<-(q_length$length[a]+as.numeric(names(seg_date_speed[i])))
    } 
    queue_length<-rbind(queue_length, q_length)
    # queue position
    for (i in 1:nrow(seg_date_speed))
    {
      df<-seg_date_speed[i, 2:ncol(seg_date_speed)]
      if (length(which(df<=threshold)) == 0)
      {
        dt<-data.frame(time=seg_date_speed[i, 1], distance = 0, Position="Start", direct=direct[d])
        queue_position<-rbind(queue_position, dt)
        dt<-data.frame(time=seg_date_speed[i, 1], distance = 0, Position="End", direct=direct[d])
        queue_position<-rbind(queue_position, dt)       
        next
      }
      b<-which(df>threshold)
      queue_head<-0
      queue_tail<-0
      for (j in 1: ncol(df))
      {
        if (is.na(df[j]))
          next
        if (df[j]<= threshold)
        {
          queue_tail<-ver_pos_dis[j]
        }
      }
      if (queue_tail > 0)
      {
        a<-which(df==min(df))
        queue_head<-max(ver_pos_dis[which(b<a)])
        if (queue_head=="-Inf") queue_head<-0
      }
      
      dt<-data.frame(time=seg_date_speed[i, 1], distance = queue_head, Position="Start", direct=direct[d])
      queue_position<-rbind(queue_position, dt)
      dt<-data.frame(time=seg_date_speed[i, 1], distance = queue_tail, Position="End", direct=direct[d])
      queue_position<-rbind(queue_position, dt)
      
      queue_head<-0
      queue_tail<-0
    }    
    #travel time
    normal_travel_time$tt<-(normal_travel_time$length/normal_travel_time$speed*60)
    date_travel_time$tt<-(date_travel_time$length/date_travel_time$speed*60)
    normal_travel_time<-aggregate(list(tt=normal_travel_time$tt), by=list(time=normal_travel_time$time), FUN=sum, na.rm=TRUE, na.action=NULL)
    date_travel_time<-aggregate(list(tt=date_travel_time$tt), by=list(time=date_travel_time$time), FUN=sum, na.rm=TRUE, na.action=NULL) 
    normal_travel_time<-data.frame(normal_travel_time, direct=direct[d], type="Historical Date")
    date_travel_time<-data.frame(date_travel_time, direct=direct[d], type="Closure Date")
    t_time<-rbind(normal_travel_time, date_travel_time)
    travel_time<-rbind(travel_time, t_time)
    # delay time
    d_time<-data.frame(time=time_range, delay=0, direct=direct[d])
    for (i in 1:length(time_range))
    {
      if (date_travel_time$tt[i]>normal_travel_time$tt[i])
        d_time$delay[i]<-(date_travel_time$tt[i]-normal_travel_time$tt[i])
    } 
    delay_time<-rbind(delay_time, d_time)
    seg_date_speed<-data.frame(seg_date_speed, direct=direct[d])
  }
  
    delay_time$mdelay<-0
    if (input$ver_be_area_select_input == 'Select first and last per corridor')
    {
      delay_time$mdelay<-as.integer(input$max_delay_allowed_be)
    }else{
      delay_time$mdelay[delay_time$direct== "NB"]<-as.integer(input$max_delay_allowed_be_i35)
      delay_time$mdelay[delay_time$direct== "SB"]<-as.integer(input$max_delay_allowed_be_i35)
      delay_time$mdelay[delay_time$direct== "EB"]<-as.integer(input$max_delay_allowed_be_183)
      delay_time$mdelay[delay_time$direct== "WB"]<-as.integer(input$max_delay_allowed_be_183)    
    }
    
    vermac_traffic$queue_length<-queue_length
    vermac_traffic$queue_position<-queue_position
    vermac_traffic$travel_time<-travel_time
    vermac_traffic$delay_time<-delay_time
    vermac_traffic$closure_count<-vermac_count_plot
    vermac_traffic$speed_plot<-vermac_speed_plot
    
}