Two sample codes calculates the impact of work zone, one data source is sensor (point speed and counts), other data source is NPMRDS (segment speed).
The calculation using sensor data includes: 

![sensor queue length](/Calculation of work zone impact/interpret_1.png "Queue length calculating by sensor data")
<br>
![sensor queue position](/Calculation of work zone impact/interpret_2.png "Queue positions and speed heatmap calculating by sensor data")
<br>
![sensor travel time](/Calculation of work zone impact/interpret_3.png "travel time calculating by sensor data")
<br>
![sensor delay time](/Calculation of work zone impact/interpret_4.png "Delay time calculating by sensor data")

The calculation using NPMRDS includes: 

![tmc queue position](/Calculation of work zone impact/tmc_plot.png "Queue positions and speed heatmap calculating by NPMRDS")




